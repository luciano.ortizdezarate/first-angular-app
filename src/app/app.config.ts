import { ApplicationConfig } from '@angular/core';
import { provideRouter } from '@angular/router'; //Agregar para activar el router en la app
import routeConfig from './app.routes'; //configuración importada desde archivo main.ts
import { provideClientHydration, provideProtractorTestingSupport } from '@angular/platform-browser';

export const appConfig: ApplicationConfig = {
  providers: [provideProtractorTestingSupport()  ,provideRouter(routeConfig), provideClientHydration()]
};
